const form = document.querySelector('form')

form.addEventListener('submit', (e) => {
  e.preventDefault()

  const valor_venda = Number(document.querySelector(".valor_venda").value);
  const valor_pago = Number(document.querySelector(".valor_pago").value);

  if (valor_pago < valor_venda) {
    return alert("Valor pago é menor que o preço");
  }

  const troco = valor_pago - valor_venda;
  const result = `Troco R$ ${troco}`;

  alert(result);
})
