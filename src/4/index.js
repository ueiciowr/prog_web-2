const form = document.querySelector(".form");

const handleSubmit = (event) => {
  event.preventDefault();
  const values = [];

  for (let i = 0; i <= 1; i++) {
    values.push(event.target[i].value);
  }

  const parcelasElement = document.createElement("span");

  const [valor, parcelas] = values;

  if(parcelas.toString().length > 2 || parcelas.match('/\./g')) return alert('Digite um número inteiro')

  if (parcelas > 10) return alert("Parcelas precisam ser menores que 10");

  let valorParcela;

  const arr = [];
  const total = [];
  const jurosCompostos = [
    0.02, 0.04, 0.06, 0.08, 0.1, 0.12, 0.14, 0.16, 0.18, 0.2,
  ];

  for (let i = 1; i <= parcelas; i++) {
    if (parcelas <= 5) {
      valorParcela = valor / parcelas;
    } else {
      juros = (valor / parcelas) * jurosCompostos[i - 1] || 0.02;
      valorParcela = valor / parcelas + juros;
    }
    arr.push(`parcela ${i} = R$ ${Number(valorParcela).toFixed(2)}`);
    total.push(valorParcela);
  }
  const valorTotal = total.reduce((acc, value) => acc += value)
  arr.push(`Total: R$ ${Number(valorTotal).toFixed(2)}`);
  parcelasElement.innerText = arr.join("\n");

  parcelasElement.className = "parcelas";
  form.appendChild(parcelasElement);
};

form.addEventListener("submit", handleSubmit);
