const form = document.querySelector('form')

form.addEventListener('submit', (e) => {
  e.preventDefault()

  const valor_venda = Number(document.querySelector(".valor_venda").value);
  const valor_pago = Number(document.querySelector(".valor_pago").value);

  if (valor_pago < valor_venda) {
    return alert("Valor pago é menor que o preço");
  }

  const nota = [100, 50, 20, 10, 5, 2, 1];

  let result;
  let troco;
  let i, valor, ct;

  troco = valor_pago - valor_venda;
  result = `Troco R$ ${troco}`;

  valor = troco;
  i = 0;
  while (valor != 0) {
    ct = (valor / nota[i]);
    if (ct != 0) {
      result = result + (`${ct} notas(s) ${nota[i]} \n`);
      valor = valor % nota[i];
    }
    i = i + 1;
  }

  alert(result);
})
