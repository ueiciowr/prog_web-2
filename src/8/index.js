const form = document.querySelector('form')

form.addEventListener('submit', (e) => {
  e.preventDefault()

  const valor_venda = Number(document.querySelector(".valor_venda").value);
  const valor_pago = Number(document.querySelector(".valor_pago").value);

  if (valor_pago < valor_venda) {
    return alert("Valor pago é menor que o preço");
  }

  const nota = [100, 50, 20, 10, 5, 2, 1];
  const centavos = [50, 25, 10, 5, 1];

  let result;
  let troco;
  let i, valor, ct;

  troco = valor_pago - valor_venda;
  result = `Troco R$ ${troco.toFixed(2)}\n\n`;

  valor = Number(troco);
  i = 0;
  while (valor != 0) {
    ct = +Number(valor / nota[i]).toFixed(1);
    if (ct != 0) {
      valor = Math.round(valor % nota[i]);
      result = result + `${ct} notas(s) de R$ ${nota[i]}\n`;
    }
    i = i + 1;
  }

  result = `${result} \n`;

  valor = +Math.round(troco);
  i = 0;
  console.log(valor);
  while (valor != 0) {
    ct = +Number(valor / centavos[i]).toFixed(1);
    if (ct != 0) {
      valor = Math.round(valor % centavos[i]);
      result = result + `${ct} moeda(s) de R$0.${centavos[i]} centavos\n`;
    }
    i = i + 1;
  }
  alert(result);
})
