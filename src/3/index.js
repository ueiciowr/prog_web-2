const form = document.querySelector(".form");

const handleSubmit = (event) => {
  event.preventDefault();
  const values = [];

  for (let i = 0; i <= 1; i++){
    values.push(event.target[i].value);
  }

  const parcelasElement = document.createElement('span')

  const [valor, parcelas] = values;

  if(parcelas > 10) return alert("Parcelas precisam ser menores que 10")
  
  const valorParcela = valor / parcelas;
  
  const arr = []
  for (let i = 1; i <= parcelas; i++){
    parcelasElement.innerHTML = `R$ ${valor} em ${parcelas}x <br>`;
    arr.push(`parcela ${i} = R$ ${valorParcela}`);
  }
  
  parcelasElement.innerText = arr.join('\n')

  parcelasElement.className = "parcelas";
  form.appendChild(parcelasElement);
};

form.addEventListener("submit", handleSubmit);
