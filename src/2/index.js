const form = document.querySelector(".form");

const handleSubmit = (event) => {
  event.preventDefault();
  const values = [];

  for (let i = 0; i <= 2; i++){
    values.push(event.target[i].value);
  }

  alert(Math.max(...values))
};

form.addEventListener("submit", handleSubmit);
