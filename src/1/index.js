const body = document.querySelector("body");

window.onload = (e) => {
  const form = document.createElement("form");
  body.appendChild(form);

  form.style.display = 'flex'
  form.style.flexFlow = 'column'
  form.style.width = '30rem'

  createInput(form);
  form.style.marginTop = "5rem"

  const button = document.createElement('button');
  const title = document.createElement('h1');
  title.innerText = "Média"
  title.style.position = "absolute"
  title.style.top = "60px";

  form.appendChild(title)

  button.type = "submit"
  button.textContent = "Enviar"
  button.style.marginTop = "2rem"
  form.appendChild(button)

  form.addEventListener('submit', (e) => handleSubmit(e, form))
};

const handleSubmit = (event, form) => {
  event.preventDefault();
  const values = [];

  for (let i = 0; i <= 3; i++){
    values.push(+event.target[i].value);
  }

  const avg = values.reduce((acc, item) => acc += item)

  const average = document.createElement('span');
  average.style.marginTop = '1rem'
  average.innerHTML = `Média: ${avg / values.length}`;
  form.appendChild(average)
}

const createInput = (form) => {
  [1, 2, 3, 4].forEach((item, i) => {
    const input = document.createElement("input");
    form.appendChild(input);
    input.placeholder = `Valor ${item}`;
    input.name = `${i}`;
    input.style.marginTop = '1rem'
  });
};
